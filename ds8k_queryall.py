from ds8k_config import customer_name, data_dir, data_extension
from openpyxl import Workbook, load_workbook
from openpyxl.styles import PatternFill, colors, Font, Color
from openpyxl.worksheet.table import Table, TableStyleInfo
from openpyxl.utils import get_column_letter
import re
import os

def main():
    ds8k_files = get_files(data_dir, data_extension)
    for i in range(len(ds8k_files)):
        ds8k_file = open(f'{data_dir}\{ds8k_files[i]}', 'r')
        command_output_dict = parse_ds8k_file(ds8k_file)
        serial_number = get_serial_number(command_output_dict)
        write_to_workbook(command_output_dict, customer_name, serial_number)

def get_files(data_dir, data_extension):
    ds8k_files = []
    for root, dirs, files in os.walk(data_dir):
        for file in files:
            if file.endswith(data_extension):
                ds8k_files.append(file)
    return ds8k_files

def xlref(row, column, zero_indexed=False):
    if zero_indexed:
        row += 1
        column += 1
    return get_column_letter(column) + str(row)

def auto_size_columns(ws):
    dims = {}
    for row in ws.rows:
        for cell in row:
            if cell.value:
                dims[cell.column_letter] = max((dims.get(cell.column_letter, 0), len(str(cell.value))))    
    for col, value in dims.items():
        ws.column_dimensions[col].width = value + 5 # Add five to account for headers with filter buttons and are wider than the values in the table

def get_serial_number(command_output_dict):
    for key, value in command_output_dict.items():
        if "showsi" in key:
            for i in value:
                for j in i:
                    if "MTS" in j:
                        serial_number = j.split("-")[1] 
    if serial_number:
        return serial_number

def parse_ds8k_file(ds8k_unparsed_file):
    lines = ds8k_unparsed_file.readlines()
    in_command = False
    command_output_dict = {}
    for line in lines:
        line = line.strip()
        if "command:" in line and in_command == False:
            line_split = line.split(": ")            
            command = line_split[1].split(' ')[0]
            command_output_list = []
            in_command = True
        elif in_command and line == "*":
            command_output_dict[command] = command_output_list
            in_command = False
        elif '=====' in line or '-----' in line or line == "*":
            pass
        elif in_command:
            line_split = line.split('|')
            command_output_list.append(line_split)
    return(command_output_dict)

def write_to_workbook(command_output_dict, customer_name, serial_number):
    workbook_name = f"{serial_number}_{customer_name}_DS8K_QUERYALL.xlsx"
    wb = Workbook()

    print(f'\nCreating Workbook: {workbook_name}')
    
    ws = wb.active
    ws.title = "TOC"
    ws.sheet_view.showGridLines = False
    toc_row = 5
    toc_column = 2
    ws.cell(row = toc_row - 3, column = toc_column, value = customer_name)
    ws.cell(row = toc_row - 2, column = toc_column, value = f"DS8000 DSCLI output from Serial Number: {serial_number}")
    ws.cell(row = toc_row, column = toc_column, value = "Table of Contents")
    ws[f'B{toc_row - 3}'].font = Font(size=18, bold=True)
    ws[f'B{toc_row - 2}'].font = Font(size=14, bold=True)
    ws[f'B{toc_row}'].font = Font(size=14, bold=True)
    for key, value in sorted (command_output_dict.items()):
        ws = wb.create_sheet(key)
        ws.sheet_view.showGridLines = False
        toc = wb["TOC"]
        toc_row += 1
        key = key.replace("-","_")
        toc.cell(row=toc_row, column=toc_column).value = f'=HYPERLINK("#{key}!A1", "{key}")'
        toc.cell(row=toc_row, column=toc_column).font = Font(color=colors.BLUE, underline="single")
        ws.cell(row = 1, column = 1, value = f'=HYPERLINK("#TOC!A1", "Go to Table of Contents")')
        ws.cell(row = 2, column = 1, value = key)
        ws['A1'].font = Font(color=colors.BLUE, italic=True)
        ws['A2'].font = Font(bold=True)
        starting_row = 3
        row = starting_row
        for command_list in value:
            for i in range(len(command_list)):
                _ = ws.cell(column = i + 1, row = row, value = str(command_list[i]))
            row += 1
        last_cell = xlref(ws.max_row,ws.max_column)
        table_name = f'tbl_{key}'
        tab = Table(displayName=table_name, ref= f"A3:{last_cell}")
        # Add a default style with striped rows and banded columns
        style = TableStyleInfo(name="TableStyleMedium1", showFirstColumn=False,
                            showLastColumn=False, showRowStripes=True, showColumnStripes=False)
        tab.tableStyleInfo = style
        table_rows = ws.max_row - starting_row
        if table_rows == 1:
            row_word = "row"
        else:
            row_word = "rows"
        print(f'    Building Table: {table_name} with {table_rows} {row_word}')
        if ws.max_row > starting_row:
            ws.add_table(tab)
        auto_size_columns(ws)
    wb.save(f"{data_dir}\\{workbook_name}")
    
if __name__== "__main__":
    main()